#!/usr/bin/env php
<?php // ۞ text{ encoding:utf-8; bom:no; linebreaks:unix; tabs:4; }  ۞ //

/*
* PDF-Squeeze - A script to make it easy to optimize/compress PDF files.
*
* THIS IMPROVED VERSION in PHP:
* Copyright (c) 2022, Jim S. Smith, AFWS
* All rights reserved.
*
* https:// -
* Licensed under GPL 2.0 or newer.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
*
* This version was inspired by an example called 'shrinkpdf' similar to it at:
*    https://bash.cyberciti.biz/file-management/linux-shell-script-to-reduce-pdf-file-size/
*    Published by: Vivek Gite - "nixCraft" blog, DTD: November 10, 2021.
*
* And its original version of 'shrinkpdf' from: Alfred Klomp, Copyright (c) 2014-2022, Alfred Klomp
*    Found at: http://www.alfredklomp.com/programming/shrinkpdf/
*
* REQUIREMENTS:
*
*    Ghostscript utility/library,
*    PHP-CLI 7.0 or '>',
*    Able to execute shell 'system()' calls,
*
*
* RECOMMENDED PHP VERSION: >= 7.3 (highly-suggested, anyway)
*
*/


/* *    APPLICATION INFO.   * */
$THIS_APP = basename( __FILE__ );
$VERSION = '1.2.0c';
$BUILD_DATE = 20221029;

//  Script header info.
$MYINFO = "$THIS_APP $VERSION, Build Date: $BUILD_DATE";

//  Send all output to log? ( A simple 'yes' would do nicely. ;-) )
$USAGE_LOG = '';


//  Show help text, Output to STDOUT
function show_help() {
    global $THIS_APP, $MYINFO;

    echo <<< _HELP_

$MYINFO

'$THIS_APP' - Is a useful tool for optimizing (or "compressing") PDF files to make them
              more suited, and more efficient, for sending whether via email, or just so that they
              take up less file space.

EXAMPLE: <sudo> $THIS_APP -s this-file.pdf -d that-file.pdf -i 75 -c best

( -h for help, anywhere, will stop execution and display this help text. )

FLAGS/OPTIONS:

 ! -s / source      - The Source Document. Must be a 'PDF' file-type (as with a '.pdf' file-extension),
                      and can include its filepath.

 ! -d / destination - The Destination Document. Also expected to be a 'PDF'-type, and not the same as the
                      source-file name and path. This can also include a filepath.

   -i / image-res   - Image Resolution. A means of optimizing images which are contained in the source document.
                      If not specified, defaults to '90' DPI. The higher the number DPI, the larger the image(s).

   -c / compression - Compression-level. How high the compression-ratio to compress the destination document.

          'low'    -> Default if not set. Valid arguments are: 'low', 'min', and 'minimal'.
                      Output document will have a DPI of '300'. Therefore, the file size will see a little
                      optimization.

          'medium' -> Valid arguments are: 'med', 'medium', and 'mid'.
                      Output document will have a DPI of '150'. The resulting file size will be moderately
                      optimized.

          'high'   -> Valid arguments are: 'best' and 'high'.
                      Output document will have a DPI of '72'. The resulting file size should be MUCH smaller.

   -g / gray-scale  - Convert all colors into gray-scale. - This is a 'flag' with no argument.
  ( or "grey-scale" )

   -r / replace     - Replace the destination document, with the newly-generated one? This is a 'flag' with no argument.
                      If not specified, defaults to 'no' and refuses to replace the destination if it already exists.

   -h / help        - Show this help text and stop execution.

NOTES: Entries marked with a '!' are mandatory arguments.
       IF any of the filepaths contain spaces, the whole path must be enclosed within quotes, like:

 $THIS_APP -s 'my document.pdf' -d './these files/new document.pdf' -i 80 -c mid -r

       This utility was not designed to use with wildcards in the filepath specs. For "globbing",
       use the shell 'find' command in conjunction with this utility instead.

TIPS:  You can fine tune the '-i' and '-c' argument values to get your most preferred results.
       The '-i' argument mainly deals with images. This is especially true if you are modifying
       a PDF which is a scanned document image, where each page is actually an image rather text.

       An example of using "long options" -

 $THIS_APP -source='this document.pdf' -destination='./these files/new-document.pdf' -image-res=85 -compression=best

OTHER NOTES:

       This compression utility, because it uses "lossy compression" may not be the most suitable for PDF documents
       originating from scanned hard copies. - This is because scanned documents are usually stored as graphical images
       and not typically OCR-scanned. Therefore, graphic documents may suffer greater loss of resolution after using this
       utility to compress them. Experimenting with and tweaking the "compression" and "image-res" values may be needed for
       finding the best-achievable results.

_HELP_;

    exit( 0 );
}

//  Display "usage" text - in case of bad command/arguments string.
function show_usage() {
    global $THIS_APP, $MYINFO;

    echo <<< _HELP_

$MYINFO

Reduces PDF filesize by lossy recompression, using the capabilities of Ghostscript.

Though not a guarantee that the compressed files will be as high-quality as the originals,
  This utility should still be very useful and helpful for most of your optimization needs.

USAGE:  $THIS_APP -s <source file> -d <destination file> -i <image resolution> -c <compression factor>

DEFAULTS:

  -i - '90' (DPI)
  -c - 'low'

FOR MORE HELP:  $THIS_APP -h

_HELP_;

}

//  Post formatted error message.
function post_errors( $this_message ) {
    echo "\nERROR:  $this_message\n";
}

//  Our custom shell-command execution function.
function send_to_shell( $cmd_str = '' ) {
    if ( empty( $cmd_str ) ) {
        return '';
        }

    ob_start();
    system( "$cmd_str ;" );
    $return_code = trim( ob_get_clean() );

    if ( empty( $return_code ) ) {
        return 0;
        }

    return $return_code;
}

//  Send the command, via shell-call, to the ghostscipt utility.
function shrink_file( $inFile = '', $outFile = '', $imgRes = 90, $compRatio = '/prepress', $compat_ver = '1.4', $grey_level = false ) {
    if ( empty( $inFile ) || empty( $outFile ) ) {
        return false;
        }

//  Set default values if not already specified.
    $imgRes = ( $imgRes ? $imgRes : 90 );
    $compRatio = ( $compRatio ? $compRatio : '/prepress' );

//  Do we want to convert to using gray-scale?
    if ( $grey_level ) {
        $grayScaling = <<< _GREY_SCALE_PARAMS_
-sProcessColorModel=DeviceGray \\
-sColorConversionStrategy=Gray \\
-dOverrideICC \\

_GREY_SCALE_PARAMS_;
        }

    else {
        $grayScaling = '';
        }

//  Build and then send the command-string to ghostscript.
    $cmdStr = <<< _THE_CMD_
gs  \\
-q -dNOPAUSE -dBATCH -dSAFER \\
-sDEVICE=pdfwrite \\
-dCompatibilityLevel=$compat_ver \\
-dPDFSETTINGS="$compRatio" \\
-dEmbedAllFonts=true \\
-dSubsetFonts=true \\
-dAutoRotatePages=/None \\
-dColorImageDownsampleType=/Bicubic \\
-dColorImageResolution=$imgRes \\
-dGrayImageDownsampleType=/Bicubic \\
-dGrayImageResolution=$imgRes \\
-dMonoImageDownsampleType=/Subsample \\
-dMonoImageResolution=$imgRes \\
-sOutputFile="$outFile" \\
$grayScaling"$inFile" \\

_THE_CMD_;

    return send_to_shell( $cmdStr );

//  die( "CMD-String: $cmdStr\n\n" ) ;  //  DEBUGGING/TESTING.
}

//  See if we successfully compressed the PDF.
function compare_files( $inFile = '', $outFile = '' ) {
    if ( empty( $inFile ) || empty( $outFile ) ) {
        return false;
        }

//  Make sure when still have BOTH files.
    if ( ! file_exists( $inFile ) || ! file_exists( $outFile ) ) {
        return false;
        }

    $source = filesize( $inFile );
    $target = filesize( $outFile );

//  Test to see if the destination file is actually smaller than what we started with.
    if ( $target < $source ) {
        if ( $GLOBALS['USAGE_LOG'] == 'yes' ) {
            echo "\n";
            echo "Size of source-file: '$inFile' is:\n$source bytes\n\n";
            echo "Size of destination-file: '$outFile' is:\n$target bytes\n\n";
            }

//      Calculate and return the difference in size of bytes.
        return intval( $source - $target );
        }

//  IF destination file is larger, then no point in keeping it.
    else {
        post_errors( "'$outFile' is larger than '$inFile'! Deleting '$outFile'." );
        unlink( $outFile );
        return 0;
        }

    return 0;
}

/*
* A new answer to a few of the weaknesses in PHP's "getopt()" implementation.
*
* WEAKNESSES ANSWERED:
*
* 1. How to handle invalid options ($flags),
* 2. How to handle duplicate options instances ($duplics),
*/
function new_getopt( $short_opts = '', $long_opts = [], $flag = false, $duplics = false ) {
    if ( empty( $short_opts ) && ( ! count( $long_opts ) || ! is_array( $long_opts ) ) ) {
        return false;
        }

//  A stack-array, to hold the available options that are allowed.
    $these_opts = [];

    if ( ! empty( $short_opts ) ) {
        foreach ( str_split( $short_opts ) as $this_shortie ) {
            if ( $this_shortie != ':' ) {
                $these_opts[] = $this_shortie;
                }
            }
        }

    if ( is_array( $long_opts ) && count( $long_opts ) ) {
        foreach ( $long_opts as $this_longie ) {
            $these_opts[] = str_replace( ':', '', $this_longie );
            }
        }

//  $found_args = [];

//  Parse the commandline options via PHP's "argv" array.
    foreach ( $GLOBALS['argv'] as $this_arg ) {

//      Only want the actual "options" switches, not the argument-values.
        if ( strlen( $this_arg ) > 1 && $this_arg[0] == '-' ) {
            $new_arg = str_replace( '-', '', $this_arg );
            $new_arg = ( strpos( $new_arg, '=' ) !== false ? strstr( $new_arg, '=', true ) : $new_arg );

//          Oops! Found an invalid option!
            if ( ! in_array( $new_arg, $these_opts ) ) {
//              $found_args[] = $new_arg;

                $this_arg = ( strpos( $this_arg, '=' ) !== false ? strstr( $this_arg, '=', true ) : $this_arg );

                // Found an invalid option!
                echo "Invalid argument specified '$this_arg'!\n";
                return 22; // Linux error return code for "invalid argument".
                }
            }
        }

//  If all is good, fall-through to the regular "getopt()" function.
    return getopt( $short_opts, $long_opts );
}


/* * *  START OF MAIN PROGRAM.  * * */

//  First, check if we have Ghostscript installed. We need this!
if ( send_to_shell( "which ghostscript" ) == '' ) {
    post_errors( "This script requires Ghostscript which was not detected or not installed!\n\nSuggest installing it with: <sudo> apt-get install ghostscript" );
    exit( 65 );
    }

//  Set up default argument values.
$IFILE = '';
$OFILE = '';
$IRES = 90;
$COMP = 'low';
$REPLACE = false;
$GRAYSCALING = false;
$RES = 0;

//  Value boundaries for image-resolution argument.
$RES_LO = 0;
$RES_HI = 101;

//  It seems PHP also has a very weak "getopt" feature! - So we use our own drop-in solution.
$theseOptions = new_getopt( 's:d:i:c:grh',
    [ 'source:', 'destination:', 'image-res:', 'compression:', 'grey-scale', 'replace', 'help' ] );

//  If returned is NOT an array, something bad happened.
if ( ! is_array( $theseOptions ) ) {
    exit( $theseOptions );
    }

//  The real magic of parsing commandline options is here!
foreach ( $theseOptions as $thisOpt => $optValue ) {

//  Protect against repeated iterations of any options (first iteration used, only).
    if ( is_array( $optValue ) ) {
        post_errors( "Option '$thisOpt' was repeated! Only first instance will be used." );
        $optValue = $optValue[0]; // To make sure ALL options values are NOT arrays!
        }

    switch ( $thisOpt ) {

        case 'source':
        case 's':
            if ( ! $IFILE = $optValue ) {
                post_errors( "Missing value for Source File!" );
                exit( 9 );
                }

            if ( ! preg_match( '!(\.pdf)$!', $IFILE ) ) {
                post_errors( "Source file path is NOT for a PDF!\n\n'$IFILE' given." );
                exit( 22 );
                }

            if ( ! file_exists( $IFILE ) ) {
                post_errors( "'$IFILE' does not exist!" );
                exit( 2 );
                }
        break;

        case 'destination':
        case 'd':
            if ( ! $OFILE = $optValue ) {
                post_errors( "Missing value for Destination File!" );
                exit( 9 );
                }

            if ( /* $OFILE != '-' && */ ! preg_match( '!(\.pdf)$!', $OFILE ) ) {
                post_errors( "Destination file path is NOT for a PDF!\n\n'$OFILE' given." );
                exit( 22 );
                }
        break;

        case 'image-res':
        case 'i':
            $IRES = (int)$optValue;

            if ( $IRES < $RES_LO || $IRES > $RES_HI ) {
                post_errors( "Bad value for Image Resolution!\n\nNeeds to be within range: $RES_LO < n < $RES_HI." );
                exit( 34 );
                }

            $RES = (int)$IRES;
        break;

        case 'compression':
        case 'c':
            if ( ! $COMP = $optValue ) {
                post_errors( "Compression-ratio requires a value! Default of 'minimum' will be used." );
                $COMP = 'min';
                }
        break;

        case 'gray-scale':
        case 'grey-scale':
        case 'g':
            $GRAYSCALING = true;
        break;

        case 'replace':
        case 'r':
            $REPLACE = true;
        break;

        case 'help':
        case 'h':
            show_help();
        break;
/*
        default : // Aww, booo! PHP doesn't use this to detect invalid options!
            post_errors( 'Unrecognized option used!' );
            show_usage();
            exit( 22 );
*/
        }
    }

//  Sanity Check - are there at least two arguments with 2 values?
if ( @$argc < 4 /* && @count( $theseOptions ) < 2 */ ) {
    post_errors( 'Too few arguments! Need at least two to work with.' );
    show_usage();
    exit( 140 );
    }

//  BOTH file-paths need to be unique.
if ( $IFILE == $OFILE ) {
    post_errors( "Both Source and Destination Filenames need to be different!" );
    exit( 141 );
    }

//  Do we replace an already existing destination file (unless it is '-', which means "send to STDOUT")?
if ( $OFILE != '-' && file_exists( $OFILE ) ) {
    if ( $REPLACE ) {
        echo "Opted to replace '$OFILE' with a new copy.\n\n";
        unlink( $OFILE );
        }

//  If the 'replace' option was NOT specified, then refuse to replace the target file if it exists.
    else {
        post_errors( "'$OFILE' already exists!\n\n Use '-r' ('replace') option to force its replacement." );
        exit( 17 );
        }
    }

//  If the target file does not exist, then no point in having 'replace' option say otherwise.
else {
    $REPLACE = false;
    }


// Set the compression-level flag.
switch ( $COMP ) {

//  For 300 DPI for minimum compression.
    case 'low':
    case 'min':
    case 'minimal':
        $COMP = '/prepress';
    break;

//  For 150 DPI for moderate compression.
    case 'med':
    case 'medium':
    case 'mid':
        $COMP = '/ebook';
    break;

//  For 72 DPI at highest compression.
    case 'best':
    case 'high':
        $COMP = '/screen';
    break;

//  Default 'fall-through' value: "Minimum-compression-ratio".
    default :
        post_errors( "Bad option value for compression! Using default value of minimal." );
        $COMP = '/prepress';
    break;
}

$doFlag = 0;

//  Detect the PDF version. ( "PDF"-signature should be at the first byte of the file. )
if ( ! file_exists( $IFILE ) || ! ( $GET_CHUNK = file_get_contents( $IFILE, false, null, 0, 16 ) ) ) {
    post_errors( "Could not read the contents of '$IFILE', or it doesn't exist!" );
    exit( 2 );
    }

if ( preg_match( "@^%PDF-([0-9]+\.[0-9]+)@", $GET_CHUNK, $m ) ) {
    $THIS_VERSION = "$m[1]";
    echo "'$IFILE' appears to be PDF Version: $THIS_VERSION\n\n";
    }

else {
    post_errors( "Could not determine the PDF version! Are you sure '$IFILE' is really is a PDF?" );
    exit( 9 );
    }


//  "Do the deed."
if ( $doFlag = shrink_file( $IFILE, $OFILE, $RES, $COMP, $THIS_VERSION, $GRAYSCALING ) ) {
    post_errors( "An error occurred when attempting to optimize '$IFILE'!" );
    exit( 5 );
    }

//  Compare the sizes of the two files.
$result = compare_files( $IFILE, $OFILE );

//  Send out the statistics.
if ( $result > 0 && $USAGE_LOG == 'yes' ) {
    clearstatcache();
    $by_percent = sprintf( "%01.02f", 100 * ( floatval( filesize( $IFILE ) / filesize( $OFILE ) ) ) );
    echo "Saved: $result bytes ( %$by_percent reduction in file-size ).\n\n";
    }

elseif ( file_exists( $OFILE ) ) {
    echo "Done! '$OFILE' is ready.\n\n";
    }

else {
	post_errors( "Oops! An error must have occurred when attempting to optimize '$IFILE'." );
	exit( 5 );
	}

